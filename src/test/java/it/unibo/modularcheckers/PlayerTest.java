package it.unibo.modularcheckers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;

import it.unibo.modularcheckers.model.BotPlayer;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.ChessboardImpl;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Player;
import it.unibo.modularcheckers.model.RandomPlayer;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.StepImpl;
import it.unibo.modularcheckers.util.CoordinateFactory;
import it.unibo.modularcheckers.util.InfiniteXCoordinateFactory;

/**
 * Basic Player Test.
 */
public class PlayerTest {

    private final CoordinateFactory factory = new InfiniteXCoordinateFactory();

    private static final int INSERTIONS = 100;

    /**
     * test Player Equals.
     */
    @Test
    public void testPlayer() {
        final Player p0 = new RandomPlayer("Mario");
        final Player p1 = new RandomPlayer("Susanna");
        final Player p2 = new RandomPlayer("Gianna");
        final Player p3 = new RandomPlayer("Mario");
        assertFalse("Testing Equals for p0 and p1.", p0.equals(p1));
        assertFalse("Testing Equals for p0 and p2.", p0.equals(p2));
        assertEquals("Testing Equals for p0 and p3.", p0, p3);
        assertFalse("Testing Equals for p1 and p2.", p1.equals(p2));
        assertFalse("Testing Equals for p1 and p3.", p1.equals(p3));
        assertFalse("Testing Equals for p1 and p3.", p2.equals(p3));
        System.out.println("testPlayer Passed! OK!");
    }

    /**
     * test BotPlayer random selection.
     */
    @Test
    public void botSelectRandomPiece() {
        final BotPlayer p0 = new RandomPlayer("Gennaro");
        final Chessboard board = new ChessboardImpl();
        final Set<Coordinate> movable = new HashSet<>();
        for (int i = 0; i < INSERTIONS; i++) {
            movable.add(factory.next());
        }
        for (int i = 0; i < INSERTIONS; i++) {
            assertTrue("Player can choose pieces that can't move",
                    movable.contains(p0.selectedPiece(board, movable).get()));
        }
    }

    /**
     * test BotPlayer random stepMake.
     */
    @Test
    public void botMoveRandom() {
        final BotPlayer p0 = new RandomPlayer("Gandalf");
        final Chessboard board = new ChessboardImpl();
        final List<Step> steps = new ArrayList<>();
        for (int i = 0; i < INSERTIONS; i++) {
            steps.add(new StepImpl(factory.next()));
        }
        final List<Coordinate> stepsCoord = steps.stream().map(s -> s.getCoordinate()).collect(Collectors.toList());
        for (int i = 0; i < INSERTIONS; i++) {
            final Coordinate chosenCoord = p0.chooseStep(board, steps);
            assertTrue("Player can choose steps that can't choose", stepsCoord.contains(chosenCoord));
        }
    }
}
