package it.unibo.modularcheckers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import it.unibo.modularcheckers.checkers.model.piece.King;
import it.unibo.modularcheckers.checkers.model.piece.Man;
import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.piece.Piece;
import it.unibo.modularcheckers.model.piece.PieceType;

/**
* Test class for King class and Man class.
* CHECKSTYLE: MagicNumber OFF
*/
public class PieceTest {


    /**
     * Testing color.
     */
    @Test
    public void testBasic() {
        final Piece p = new Man(Color.WHITE);
        assertEquals("Color change", Color.WHITE, p.getColor());
    }

    /**
     * Testing Man.
     */
    @Test
    public void testMan() {
        final Piece p = new Man(Color.BLACK);
        assertEquals("PieceType must be a man", PieceType.MAN, p.getType());
        final Stream<Coordinate> moveSet = p.getMoveSet();
        final List<Coordinate> list = moveSet.collect(Collectors.toList());
        assertEquals("Man should have 2 moves", 2, list.size());
        final List<Coordinate> expectedList = new ArrayList<Coordinate>(2);
        expectedList.add(new Coordinate(1, 1));
        expectedList.add(new Coordinate(-1, 1));
        assertTrue("Man can't make these moves", list.containsAll(expectedList));
    }

    /**
     * Testing King.
     */
    @Test
    public void testKing() {
        final Piece p = new King(Color.WHITE);
        assertEquals("PieceType must be a king", PieceType.KING, p.getType());
        final Stream<Coordinate> moveSet = p.getMoveSet();
        final List<Coordinate> list = moveSet.collect(Collectors.toList());
        assertEquals("King should have 4 moves", 4, list.size());
        final List<Coordinate> expectedList = new ArrayList<Coordinate>(4);
        expectedList.add(new Coordinate(1, 1));
        expectedList.add(new Coordinate(-1, 1));
        expectedList.add(new Coordinate(1, -1));
        expectedList.add(new Coordinate(-1, -1));
        assertTrue("King can't make these moves", list.containsAll(expectedList));
    }

}
