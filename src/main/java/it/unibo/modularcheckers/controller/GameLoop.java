package it.unibo.modularcheckers.controller;

/**
 * The game repeats itself over and over. GameLoop can be seen as a consecutive
 * sequence of actions that every player performs each turn. It's the Controller
 * of the MVC pattern.
 */
public interface GameLoop {

    /**
     * Start the loop of the relative Game.
     */
    void startLoop();
}
