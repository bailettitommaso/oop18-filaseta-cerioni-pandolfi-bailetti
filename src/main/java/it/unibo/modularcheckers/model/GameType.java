package it.unibo.modularcheckers.model;

/**
 * All the playable Game.
 */
public enum GameType {
    /**
     * The game checkers. Will use Checkers.
     */
    CHECKERS,
    /**
     * The game chess. Will use ChessEngine.
     */
    CHESS;
}
