package it.unibo.modularcheckers.model.move;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.base.Optional;

/**
 * Basic implementation of Tree.
 * 
 * @param <X> the generic value of the root.
 */
public class TreeImpl<X> implements Tree<X> {

    private final X root;
    private final List<Tree<X>> children;

    /**
     * Create a Tree and its children.
     * 
     * @param root     the value of the root.
     * @param children all the sub-tree.
     */
    public TreeImpl(final X root, final List<Tree<X>> children) {
        this.root = root;
        this.children = children;
    }

    /**
     * Create a Tree without children.
     * 
     * @param root the value of the root.
     */
    public TreeImpl(final X root) {
        this.root = root;
        this.children = new ArrayList<Tree<X>>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public X getRoot() {
        return root;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tree<X>> getChildren() {
        return children;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Tree<X>> getAllNodes() {
        final List<Tree<X>> nodes = new ArrayList<>();
        nodes.add(this);
        this.getChildren().stream().forEach(n -> {
            nodes.addAll(n.getAllNodes());
        });
        return nodes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<X> getAllValues() {
        return getAllNodes().stream().map(t -> t.getRoot()).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<X> getFirstChildren() {
        return this.getChildren().stream().map(t -> t.getRoot()).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int height() {
        if (this.getChildren().size() > 0) {
            return this.getChildren().stream().mapToInt(t -> t.height() + 1).max().getAsInt();
        } else {
            return 1;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean balanceToHeight(final int maxHeight) {
        for (int i = 0; i < getChildren().size(); i++) {
            if (!getChildren().get(i).balanceToHeight(maxHeight - 1)) {
                this.getChildren().remove(i);
                i--;
            }
        }
        if (this.getChildren().size() > 0) {
            return true;
        }
        return maxHeight <= 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Integer> levelOfFirstAppearance(final Predicate<Tree<X>> condition) {
        List<Tree<X>> list = new ArrayList<Tree<X>>();
        list.add(this);
        int level = 1;
        while (!list.isEmpty()) {
            if (list.stream().filter(condition).findAny().isPresent()) {
                return Optional.of(level);
            } else {
                list = list.stream().flatMap(l -> l.getChildren().stream()).collect(Collectors.toList());
                level++;
            }
        }
        return Optional.absent();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int numberOfNodesForCondition(final Predicate<Tree<X>> condition) {
        return (condition.test(this) ? 1 : 0)
                + this.getChildren().stream().mapToInt(t -> t.numberOfNodesForCondition(condition)).sum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((children == null) ? 0 : children.hashCode());
        result = prime * result + ((root == null) ? 0 : root.hashCode());
        return result;
    }

    /**
     * {@inheritDoc} BUT the list is checked in a different way.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked") // TODO
        final TreeImpl<X> other = (TreeImpl<X>) obj;
        if (children == null) {
            if (other.children != null) {
                return false;
            }
        } else if (children.size() != other.children.size() || !children.containsAll(other.children)) {
            return false;
        }
        if (root == null) {
            if (other.root != null) {
                return false;
            }
        } else if (!root.equals(other.root)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "{TreeImpl Root:" + root.toString() + ", Children[" + children.size() + "] =" + children.toString()
                + "}";
    }

}
