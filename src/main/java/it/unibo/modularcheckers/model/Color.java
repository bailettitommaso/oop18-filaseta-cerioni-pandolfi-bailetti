package it.unibo.modularcheckers.model;

/**
 * Color Enum.
 * Colors that can be used.
 */
public enum Color {
     /**
     * White color.
     */
    WHITE,
    /**
     * Black color.
     */
    BLACK;
}
