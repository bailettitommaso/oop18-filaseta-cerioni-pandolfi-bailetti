package it.unibo.modularcheckers.model.piece;

/**
 * Type Enum.
 * Types that can be used.
 */
public enum PieceType {
    /**
     * Man Type.
     */
    MAN,
    /**
     * King Type.
     */
    KING
}
