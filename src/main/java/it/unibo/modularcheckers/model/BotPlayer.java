package it.unibo.modularcheckers.model;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import it.unibo.modularcheckers.model.move.Step;

/**
 * The automated type of player.
 */
public interface BotPlayer extends Player {

    /**
     * Select a piece to move on the Chessboard.
     * 
     * @param chessboard    The actual chessboard containing the status of the game.
     * @param movablePieces A set containing all the coordinates of the pieces thta
     *                      can move on the Chessboard.
     * @return the coordinate of the piece that will move.
     */
    Optional<Coordinate> selectedPiece(Chessboard chessboard, Set<Coordinate> movablePieces);

    /**
     * The player choose the step from all the possible pool of legal steps. Note: A
     * move is made of 2 or more steps.
     * 
     * @param chessboard The actual chessboard containing the status of the game.
     * @param steps      the possible steps for the selected piece.
     * @return the Coordinate where the piece is moving.
     * 
     */
    Coordinate chooseStep(Chessboard chessboard, List<Step> steps);
}
