package it.unibo.modularcheckers.util;

import java.io.InputStream;

/**
 * Contains the files name, used in Serialization to recover configuration
 * files.
 */
public final class ConfPathHelper {

    // Seems that resource INSIDE Java works with linux-like separators, do not attempt to set Windows file separators.
    private static final String FILE_SEPARATOR = "/";

    private static final String BASE_DIR = "gameData";
    private static final String CHECKERS_SERIALIZED = "checkersBoard";
    private static final String COLORS_SERIALIZED = "colors";
    private static final String FILE_EXTENSION = ".ser";

    private ConfPathHelper() {

    }

    /**
     * Returns the URL for the Serialized Colors resource.
     *
     * @return URL pointing the resource.
     */
    public static InputStream getColorsPath() {
        return ConfPathHelper.class.getResourceAsStream(FILE_SEPARATOR + BASE_DIR + FILE_SEPARATOR
                + COLORS_SERIALIZED + FILE_EXTENSION);
    }

    /**
     * Returns the URL for the Serialized Checkers base chessboard resource.
     *
     * @return URL pointing the resource.
     */
    public static InputStream getCheckersPath() {
        return ConfPathHelper.class.getResourceAsStream(FILE_SEPARATOR + BASE_DIR + FILE_SEPARATOR
                + CHECKERS_SERIALIZED + FILE_EXTENSION);
    }

}
