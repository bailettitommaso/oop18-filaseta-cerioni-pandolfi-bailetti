package it.unibo.modularcheckers.view.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import it.unibo.modularcheckers.model.Chessboard;

/**
 * Build the Grid for all the buttons of the chess board.
 */
public class ChessBoardComponent extends JPanel {

    private static final long serialVersionUID = -6766149699188621419L;
    private final Color opaque = new Color(204, 119, 34);

    /**
     * Creates the panel and the layout of the chess board.
     *
     * @param chessboard the gametable to be rendered
     */
    public ChessBoardComponent(final Chessboard chessboard) {
        super(new GridLayout(chessboard.getSize().getX(), chessboard.getSize().getY()));
        setBorder(new CompoundBorder(new EmptyBorder(8, 8, 8, 8), new LineBorder(Color.BLACK)));
        setBackground(opaque);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Dimension getPreferredSize() {
        final Dimension d = super.getPreferredSize();
        Dimension prefSize;
        final Component c = getParent();
        if (c == null) {
            prefSize = new Dimension((int) d.getWidth(), (int) d.getHeight());
        } else if (c.getWidth() > d.getWidth() && c.getHeight() > d.getHeight()) {
            prefSize = c.getSize();
        } else {
            prefSize = d;
        }
        final int w = (int) prefSize.getWidth();
        final int h = (int) prefSize.getHeight();
        // the smaller of the two sizes
        final int s = (w > h ? h : w);
        return new Dimension(s, s);
    }
}
