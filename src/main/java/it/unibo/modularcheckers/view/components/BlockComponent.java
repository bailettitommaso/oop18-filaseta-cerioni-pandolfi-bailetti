package it.unibo.modularcheckers.view.components;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.util.SpriteSelector;

import javax.swing.Icon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Insets;

/**
 * JButton for the Chess Board.
 */
public class BlockComponent extends JButton {

    private static final long serialVersionUID = -2897691187240352157L;
    private final SpriteSelector spriteSelector = new SpriteSelector();
    private final Color selectableColor = new Color(163, 200, 152);
    private boolean nextMove;
    private Block block;

    /**
     * Build it easily.
     */
    public BlockComponent() {
        super();
        this.nextMove = false;
        setMargin(new Insets(0, 0, 0, 0));
        setOpaque(true);
        setBorderPainted(false);
    }

    /**
     * Overrides the getBackground, so we can actually use all bool stuff as a switch.
     * @return color of the background
     */
    @Override
    public Color getBackground() {
        return nextMove ? selectableColor : super.getBackground();
    }

    /**
     * Override the image based on the block hold.
     * @return the Icon to be viewed.
     */
    @Override
    public Icon getIcon() {
        if (getBlock() != null && getBlock().pieceExists()) {
            return spriteSelector.getSprite(new Pair<>(getBlock().getPiece().get().getType(), getBlock().getPiece().get().getColor()));
        } else {
            return super.getIcon();
        }
    }

    /**
     * Setter for isNextMove.
     * @param isNextMove boolean
     */
    public void setNextMove(final boolean isNextMove) {
        this.nextMove = isNextMove;
        repaint();
    }

    /**
     * Getter for isNextMove.
     * @return boolean
     */
    public boolean isNextMove() {
        return this.nextMove;
    }

    /**
     * Getter for block.
     *
     * @return block of the component.
     */
    public Block getBlock() {
        return block;
    }

    /**
     * Setter for block.
     *
     * @param block to be added.
     */
    public void setBlock(final Block block) {
        this.block = block;
        repaint();
    }
}
