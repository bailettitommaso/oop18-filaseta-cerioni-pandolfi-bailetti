package it.unibo.modularcheckers.view;

/**
 * Enum class to describe the various type of players.
 */
public enum PlayerType {
    /**
     * The human player, he needs to interact with the view to choose the move.
     */
    HUMAN_PLAYER,
    /**
     * The RANDOM player, he choose the move automatically.
     */
    RANDOM_PLAYER,
    /**
     * The History player. He performs the moves from a file (the replay file). Note
     * that he can be used only to replicate game, and can't be seen as an effective
     * player.
     * 
     */
    HISTORY_PLAYER;

}
