package it.unibo.modularcheckers.checkers.model.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Optional;

import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.engine.AbstractStepTreeCalculator;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.StepImpl;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.move.TreeImpl;
import it.unibo.modularcheckers.model.piece.Piece;

/**
 * StepTreeBuilder implementation for checkers game, used for generate the Tree
 * of Step.
 */
public class CheckersStepTreeCalculator extends AbstractStepTreeCalculator {
    private final Color invertedColor;

    /**
     * Solo constructor.
     * 
     * @param invertedColor the color of the chessboard with the direction inverted.
     */
    public CheckersStepTreeCalculator(final Color invertedColor) {
        super();
        this.invertedColor = invertedColor;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Tree<Step> generateTreeStep() {
        return new TreeImpl<Step>(new StepImpl(this.getCoordinate()),
                this.internalGenerateTreeStep(this.getCoordinate(), false, new ArrayList<Coordinate>()));
    }

    /**
     * Recursive function that generate the tree of possible steps... La morte sua.
     * 
     * @param from              Starting coordinate
     * @param isEating          true if the piece has already eat
     * @param alreadyEatenPiece array of already eaten coordinate
     * @return the generated tree of possibile steps
     */
    private List<Tree<Step>> internalGenerateTreeStep(final Coordinate from, final boolean isEating,
            final List<Coordinate> alreadyEatenPiece) {
        final Stream<Coordinate> moveSet = this.getPieceRelativeMoveSet();
        return moveSet.map(c -> new Pair<Coordinate, Coordinate>(new Coordinate(c), new Coordinate(c)))
                // Map to pair for store in x relative move and in y the absolute move.
                .peek(p -> p.getY().translate(from)) // Translate absolute move to piece coordinate
                .filter(p -> coordinateInsideBorderPredicate(p.getY())) // Filter if leaving the chessboard.
                .map(p -> { // Map to Tree<step>
                    final Coordinate eatingCoordinate = new Coordinate(p.getY());
                    final Coordinate relativeMove = new Coordinate(p.getX());
                    final Coordinate finalCoordinate = new Coordinate(p.getY());
                    final Optional<Piece> optionalEatingPiece = this.getChessboard().getBlock(eatingCoordinate)
                            .getPiece();
                    if (optionalEatingPiece.isPresent()) {
                        final Piece eatingPiece = optionalEatingPiece.get();
                        if (this.getPiece().getColor() != eatingPiece.getColor()
                                && !alreadyEatenPiece.contains(eatingCoordinate)) {
                            final List<Coordinate> newAlreadyEatenPiece = new ArrayList<>(alreadyEatenPiece);
                            finalCoordinate.translate(relativeMove); // Traslate again if can eat.
                            final Step stepEating = new StepImpl(finalCoordinate,
                                    new Pair<Coordinate, Piece>(eatingCoordinate, eatingPiece));
                            newAlreadyEatenPiece.add(eatingCoordinate);
                            return new TreeImpl<Step>(stepEating,
                                    internalGenerateTreeStep(finalCoordinate, true, newAlreadyEatenPiece));
                            // After eating recall the recursive method.
                        }
                    }
                    return new TreeImpl<Step>(new StepImpl(finalCoordinate));
                }).filter(t -> !isEating || t.getRoot().getDeadPiece().isPresent()) // Filter one step move if has
                                                                                    // already eaten.
                .filter(t -> coordinateInsideBorderPredicate(t.getRoot().getCoordinate())) // Refilter if leaving the
                                                                                           // chessboard
                .filter(t -> {
                    final Coordinate finalCoordinate = t.getRoot().getCoordinate();
                    return !this.getChessboard().getBlock(finalCoordinate).pieceExists()
                            || alreadyEatenPiece.contains(finalCoordinate)
                            || finalCoordinate.equals(this.getCoordinate());
                }) // Filter if there is another piece after the eat
                .collect(Collectors.toList());
    }

    private boolean coordinateInsideBorderPredicate(final Coordinate c) {
        return c.isInsideBorders(0, 0, this.getChessboard().getSize().getX() - 1,
                this.getChessboard().getSize().getY() - 1);
    }

    private Stream<Coordinate> getPieceRelativeMoveSet() {
        Stream<Coordinate> moveSet = this.getPiece().getMoveSet();

        if (this.getPiece().getColor() == this.invertedColor) {
            moveSet = moveSet.peek(Coordinate::invertY); // Invert Y Axis
        }
        return moveSet;
    }

}
